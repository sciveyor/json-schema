# Sciveyor JSON Schema

This repository contains a single file, a JSON schema (currently in [JSON Schema 2019-09 draft format](https://json-schema.org/draft/2019-09/json-schema-core.html)) that encodes the [canonical document schema for Sciveyor.](https://data.sciveyor.com/schema)

## License

This schema is copyright © 2021 Charles H. Pence, and published under [Creative Commons CC-BY 4.0.](https://creativecommons.org/licenses/by/4.0/)
